package co.utz.amaysimtest;

import android.app.Application;

import co.utz.amaysimtest.model.AmaysimData;
import co.utz.amaysimtest.parser.AmaysimDataParser;

/**
 * Created by noahutzurrum on 29/12/2016.
 */
public class AmaysimApplication extends Application {

    private static final String FILE_NAME_COLLECTION = "collection.json";

    private static AmaysimApplication application;

    public static AmaysimApplication getInstance() {
        return application;
    }

    private AmaysimData mAmaysimData;

    public AmaysimData getAmaysimData() {
        return mAmaysimData;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        mAmaysimData = AmaysimDataParser.getData(this, FILE_NAME_COLLECTION);
    }
}
