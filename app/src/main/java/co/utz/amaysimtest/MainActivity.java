package co.utz.amaysimtest;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import co.utz.amaysimtest.fragment.CustomerInfoFragment;
import co.utz.amaysimtest.model.Account;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Display Name to Action Bar
        Account account = AmaysimApplication.getInstance().getAmaysimData().getAccount();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(account.getDisplayName());
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new CustomerInfoFragment())
                .commit();
    }

}
