package co.utz.amaysimtest.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import co.utz.amaysimtest.AmaysimApplication;
import co.utz.amaysimtest.R;
import co.utz.amaysimtest.model.Account;
import co.utz.amaysimtest.model.Product;
import co.utz.amaysimtest.model.Subscription;

/**
 * A placeholder fragment containing a simple view.
 */
public class CustomerInfoFragment extends Fragment {

    public CustomerInfoFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_info, container, false);

        // Subscription Info Fields
        TextView tvBalance = (TextView) view.findViewById(R.id.subscription_balance);
        TextView tvExpiry = (TextView) view.findViewById(R.id.subscription_expiry);
        TextView tvRenewal = (TextView) view.findViewById(R.id.subscription_renewal);
        TextView tvPrimary = (TextView) view.findViewById(R.id.subscription_primary);

        // Product Info Fields
        TextView tvProductName = (TextView) view.findViewById(R.id.product_name);
        TextView tvProductPrice = (TextView) view.findViewById(R.id.product_price);
        TextView tvProductUnliText = (TextView) view.findViewById(R.id.product_unlimited_text);
        TextView tvProductUnliTalk = (TextView) view.findViewById(R.id.product_unlimited_talk);
        TextView tvProductUnliTextInt = (TextView) view.findViewById(R.id.product_unlimited_text_international);
        TextView tvProductUnliTalkInt = (TextView) view.findViewById(R.id.product_unlimited_talk_international);

        // Account Info
        TextView tvPaymentType = (TextView) view.findViewById(R.id.account_payment_type);
        TextView tvEmail = (TextView) view.findViewById(R.id.account_email);
        TextView tvBirthdate = (TextView) view.findViewById(R.id.account_birthdate);
        TextView tvContact = (TextView) view.findViewById(R.id.account_contact);

        // Set Subscription Info
        Subscription subscription = AmaysimApplication.getInstance().getAmaysimData().getSubscription();
        tvBalance.setText(subscription.getDisplayBalance().toString());
        tvExpiry.setText(subscription.getExpiryDate());
        tvRenewal.setText(subscription.getAutoRenewal());
        tvPrimary.setText(subscription.getIsPrimary());

        // Set Product Info
        Product product = AmaysimApplication.getInstance().getAmaysimData().getProduct();
        tvProductName.setText(product.getName());
        tvProductPrice.setText(product.getPrice().toString());
        tvProductUnliText.setText(product.getDisplayUnlimitedText());
        tvProductUnliTalk.setText(product.getDisplayUnlimitedTalk());
        tvProductUnliTextInt.setText(product.getDisplayUnlimitedTextInternational());
        tvProductUnliTalkInt.setText(product.getDisplayUnlimitedTalkInternational());

        // Set Account Info
        Account account = AmaysimApplication.getInstance().getAmaysimData().getAccount();
        tvPaymentType.setText(account.getPaymentType());
        tvEmail.setText(account.getEmail());
        tvBirthdate.setText(account.getBirthDate());
        tvContact.setText(account.getContactNumber());

        return view;
    }
}
