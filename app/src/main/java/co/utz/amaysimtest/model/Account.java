package co.utz.amaysimtest.model;

import java.util.Locale;

/**
 * Created by noahutzurrum on 29/12/2016.
 */
public class Account {
    private String mEmail;
    private String mTitle;
    private String mFirstName;
    private String mLastName;
    private String mContactNumber;
    private String mBirthDate;
    private String mPaymentType;

    public Account() {
    }

    public String getDisplayName() {
        return String.format(Locale.getDefault(), "%s %s %s", mTitle, mFirstName, mLastName);
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getContactNumber() {
        return mContactNumber;
    }

    public void setContactNumber(String contactNumber) {
        mContactNumber = contactNumber;
    }

    public void setBirthDate(String birthDate) {
        mBirthDate = birthDate;
    }

    public String getBirthDate() {
        return mBirthDate;
    }

    public void setPaymentType(String paymentType) {
        mPaymentType = paymentType;
    }

    public String getPaymentType() {
        return mPaymentType;
    }
}
