package co.utz.amaysimtest.model;

/**
 * Created by noahutzurrum on 29/12/2016.
 */
public class AmaysimData {
    private Account mAccount;
    private Subscription mSubscription;
    private Product mProduct;

    public AmaysimData() {
        mAccount = new Account();
        mSubscription = new Subscription();
        mProduct = new Product();
    }

    public Account getAccount() {
        return mAccount;
    }

    public void setAccount(Account account) {
        mAccount = account;
    }

    public Subscription getSubscription() {
        return mSubscription;
    }

    public void setSubscription(Subscription subscription) {
        mSubscription = subscription;
    }

    public Product getProduct() {
        return mProduct;
    }

    public void setProduct(Product product) {
        mProduct = product;
    }
}
