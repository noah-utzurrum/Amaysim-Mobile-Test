package co.utz.amaysimtest.model;

import java.util.Locale;

/**
 * Created by noahutzurrum on 29/12/2016.
 */
public class CurrencyAmount {
    private int mPrice;

    public CurrencyAmount(int price) {
        mPrice = price;
    }

    @Override
    public String toString() {
        return String.format(Locale.getDefault(), "US$ %.2f", (float) mPrice / 100);
    }
}
