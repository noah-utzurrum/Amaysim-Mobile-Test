package co.utz.amaysimtest.model;

import java.util.Locale;

/**
 * Created by noahutzurrum on 29/12/2016.
 */
public class DataBalance {
    private final int mDataBalance;

    public DataBalance(String dataBalance) {
        mDataBalance = Integer.parseInt(dataBalance);
    }

    @Override
    public String toString() {
        return String.format(Locale.getDefault(), "%.2f GB", (float) mDataBalance / 1000);
    }
}
