package co.utz.amaysimtest.model;

/**
 * Created by noahutzurrum on 29/12/2016.
 */
public class Product {
    private String mName;
    private boolean mIsUnlimitedText;
    private boolean mIsUnlimitedTalk;
    private boolean mIsUnlimitedTextInternational;
    private boolean mIsUnlimitedTalkInternational;
    private CurrencyAmount mPrice;

    public Product() {
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getDisplayUnlimitedText() {
        return mIsUnlimitedText ? "YES" : "NO";
    }

    public void setUnlimitedText(boolean unlimitedText) {
        mIsUnlimitedText = unlimitedText;
    }

    public String getDisplayUnlimitedTalk() {
        return mIsUnlimitedTalk ? "YES" : "NO";
    }

    public void setUnlimitedTalk(boolean unlimitedTalk) {
        mIsUnlimitedTalk = unlimitedTalk;
    }

    public String getDisplayUnlimitedTextInternational() {
        return mIsUnlimitedTextInternational ? "YES" : "NO";
    }

    public void setUnlimitedTextInternational(boolean unlimitedTextInternational) {
        mIsUnlimitedTextInternational = unlimitedTextInternational;
    }

    public String getDisplayUnlimitedTalkInternational() {
        return mIsUnlimitedTalkInternational ? "YES" : "NO";
    }

    public void setUnlimitedTalkInternational(boolean unlimitedTalkInternational) {
        mIsUnlimitedTalkInternational = unlimitedTalkInternational;
    }

    public CurrencyAmount getPrice() {
        return mPrice;
    }

    public void setPrice(int price) {
        mPrice = new CurrencyAmount(price);
    }
}
