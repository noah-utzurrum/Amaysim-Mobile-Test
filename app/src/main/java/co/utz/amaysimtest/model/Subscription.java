package co.utz.amaysimtest.model;

import java.util.Locale;

/**
 * Created by noahutzurrum on 29/12/2016.
 */
public class Subscription {
    private DataBalance mDataBalance;
    private String mExpiryDate;
    private boolean mIsAutoRenewal;
    private boolean mIsPrimary;

    public DataBalance getDisplayBalance() {
        return mDataBalance;
    }

    public void setDataBalance(String dataBalance) {
        mDataBalance = new DataBalance(dataBalance);
    }

    public String getExpiryDate() {
        return mExpiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        mExpiryDate = expiryDate;
    }

    public String getAutoRenewal() {
        return mIsAutoRenewal ? "YES" : "NO";
    }

    public void setAutoRenewal(boolean autoRenewal) {
        mIsAutoRenewal = autoRenewal;
    }

    public String getIsPrimary() {
        return mIsPrimary ? "YES" : "NO";
    }

    public void setIsPrimary(boolean isPrimary) {
        mIsPrimary = isPrimary;
    }
}
