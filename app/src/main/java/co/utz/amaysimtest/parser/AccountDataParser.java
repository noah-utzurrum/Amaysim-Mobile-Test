package co.utz.amaysimtest.parser;

import org.json.JSONException;
import org.json.JSONObject;

import co.utz.amaysimtest.model.Account;

/**
 * Created by noahutzurrum on 29/12/2016.
 */
public class AccountDataParser {

    public static Account getData(JSONObject jsonObject) {
        try {
            Account account = new Account();
            JSONObject attrObj = jsonObject.getJSONObject("attributes");
            account.setEmail(attrObj.getString("email-address"));
            account.setTitle(attrObj.getString("title"));
            account.setFirstName(attrObj.getString("first-name"));
            account.setLastName(attrObj.getString("last-name"));
            account.setBirthDate(attrObj.getString("date-of-birth"));
            account.setContactNumber(attrObj.getString("contact-number"));
            account.setPaymentType(attrObj.getString("payment-type"));
            return account;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
