package co.utz.amaysimtest.parser;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;

import co.utz.amaysimtest.model.AmaysimData;

/**
 * Created by noahutzurrum on 29/12/2016.
 */
public class AmaysimDataParser {

    private static String loadContentsFromFile(Context ctx, String filename) {
        try {
            InputStream is = ctx.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static AmaysimData getData(Context ctx, String filename) {
        AmaysimData data = new AmaysimData();
        String contents = loadContentsFromFile(ctx, filename);
        try {
            JSONObject jsonObject = new JSONObject(contents);
            JSONObject dataObj = jsonObject.getJSONObject("data");
            String typeData = dataObj.getString("type");
            if (typeData.equalsIgnoreCase("accounts")) {
                data.setAccount(AccountDataParser.getData(dataObj));
            }
            JSONArray includedObj = jsonObject.getJSONArray("included");
            for (int i = 0; i < includedObj.length(); i++) {
                JSONObject itemObj = includedObj.getJSONObject(i);
                String typeInclude = itemObj.getString("type");
                if (typeInclude.equalsIgnoreCase("subscriptions")) {
                    data.setSubscription(SubscriptionDataParser.getData(itemObj));
                } else if (typeInclude.equalsIgnoreCase("products")) {
                    data.setProduct(ProductDataParser.getData(itemObj));
                }
            }
            return data;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }
}
