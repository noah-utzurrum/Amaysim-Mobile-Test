package co.utz.amaysimtest.parser;

import org.json.JSONException;
import org.json.JSONObject;

import co.utz.amaysimtest.model.Product;

/**
 * Created by noahutzurrum on 29/12/2016.
 */
public class ProductDataParser {

    public static Product getData(JSONObject jsonObject) {
        try {
            Product product = new Product();
            JSONObject attrObj = jsonObject.getJSONObject("attributes");
            product.setName(attrObj.getString("name"));
            product.setUnlimitedText(attrObj.getBoolean("unlimited-text"));
            product.setUnlimitedTalk(attrObj.getBoolean("unlimited-talk"));
            product.setUnlimitedTextInternational(attrObj.getBoolean("unlimited-international-text"));
            product.setUnlimitedTalkInternational(attrObj.getBoolean("unlimited-international-talk"));
            product.setPrice(attrObj.getInt("price"));
            return product;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
