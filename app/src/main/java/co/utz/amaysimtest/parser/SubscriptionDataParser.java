package co.utz.amaysimtest.parser;

import org.json.JSONException;
import org.json.JSONObject;

import co.utz.amaysimtest.model.Subscription;

/**
 * Created by noahutzurrum on 29/12/2016.
 */
public class SubscriptionDataParser {

    public static Subscription getData(JSONObject jsonObject) {
        try {
            Subscription subscription = new Subscription();
            JSONObject attrObj = jsonObject.getJSONObject("attributes");
            subscription.setDataBalance(attrObj.getString("included-data-balance"));
            subscription.setExpiryDate(attrObj.getString("expiry-date"));
            subscription.setAutoRenewal(attrObj.getBoolean("auto-renewal"));
            subscription.setIsPrimary(attrObj.getBoolean("primary-subscription"));
            return subscription;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
